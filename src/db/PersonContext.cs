namespace Jdev.RevolvingDoor.Person.Api.Db;

using Microsoft.EntityFrameworkCore;
using Db.Models;

public class PersonContext : DbContext
{
    public DbSet<Person> People { get; set; } = default!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(@"Server=localhost;Port=5432;Database=person;UserId=postgres;Password=postgres");
    }
}