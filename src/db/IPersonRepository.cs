namespace Jdev.RevolvingDoor.Person.Api.Db;

using Jdev.RevolvingDoor.Person.Api.Db.Models;

public interface IPersonRepository
{
    Task AddAsync(Person model, CancellationToken token);
    Task<Person> GetByRefAsync(string reference, CancellationToken token);
    Task<Person> GetByNameAsync(string name, CancellationToken token);
}