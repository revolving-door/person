namespace Jdev.RevolvingDoor.Person.Api.Db;

using Jdev.RevolvingDoor.Person.Api.Db.Models;
using Microsoft.EntityFrameworkCore;

public class PersonRepository : IPersonRepository
{
    public async Task AddAsync(Person model, CancellationToken token)
    {
        using (var context = new PersonContext())
        {
            await context.AddAsync(model, token);
            await context.SaveChangesAsync(token);
        }
    }

    public async Task<Person> GetByNameAsync(string name, CancellationToken token)
    {
        using (var context = new PersonContext())
        {
            var person = await context.People.SingleAsync(p => IsTheSameName(name, p.Name));
            return person;
        }
    }

    private bool IsTheSameName(string x, string y)
    {
        // needs fleshing out...
        return x.ToLower() == y.ToLower();
    }

    public async Task<Person> GetByRefAsync(string reference, CancellationToken token)
    {
        using (var context = new PersonContext())
        {
            var person = await context.People.SingleAsync(p => p.Ref.ToString() == reference);
            return person;
        }
    }
}