namespace Jdev.RevolvingDoor.Person.Api.Db.Models
{
    public class Person : BaseModel
    {
        public Guid Ref { get; set; }
        public string Name { get; set; } = default!;
        public DateTimeOffset DateOfBirth { get; set; }
    }
}