namespace Jdev.RevolvingDoor.Person.Api.Db.Models;

public class BaseModel
{
    public int Id { get; set; }
}