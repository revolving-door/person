namespace Jdev.RevolvingDoor.Person.Api.Services;

using System.Threading.Tasks;
using Grpc.Core;
using Jdev.RevolvingDoor.Person.Api.Db;
using Jdev.RevolvingDoor.Proto.Person;
using static Jdev.RevolvingDoor.Proto.Person.GetPersonRequest;
using static Jdev.RevolvingDoor.Proto.Person.PersonService;
using Person = Person.Api.Db.Models.Person;

public class PersonService : PersonServiceBase
{
    private readonly IPersonRepository _repository;

    public PersonService(IPersonRepository repository)
    {
        _repository = repository;
    }

    public override async Task<AddPersonReply> AddPerson(AddPersonRequest request, ServerCallContext context)
    {
        // how to decide if the person is already in there
        // request name of boris johnson
        // db name of Boris Johnson, Boris Joohnson???
        await _repository.AddAsync(new Person
        {
            Name = request.Name,
            DateOfBirth = request.DateOfBirth.ToDateTimeOffset()
        }, context.CancellationToken);
        return new AddPersonReply
        {
            Success = true
        };
    }

    public override async Task<GetPersonReply> GetPerson(GetPersonRequest request, ServerCallContext context)
    {
        var person = new Person();
        switch (request.IdentifierCase)
        {
            case IdentifierOneofCase.Name:
                person = await _repository.GetByNameAsync(request.Name, context.CancellationToken);
                break;
            case IdentifierOneofCase.Ref:
                person = await _repository.GetByRefAsync(request.Ref, context.CancellationToken);
                break;
            default:
                break;
        }
        return new GetPersonReply
        {
            Name = person.Name,
            Ref = person.Ref.ToString()
        };
    }
}