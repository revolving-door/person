﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Jdev.RevolvingDoor.Person.Api.Migrations
{
    public partial class RefIncluded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Ref",
                table: "People",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ref",
                table: "People");
        }
    }
}
