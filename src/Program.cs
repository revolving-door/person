using Jdev.RevolvingDoor.Person.Api.Db;
using Jdev.RevolvingDoor.Person.Api.Services;

var builder = WebApplication.CreateBuilder(args);

var s = builder.Services;

s.AddGrpc();
s.AddGrpcReflection();

s.AddSingleton<IPersonRepository, PersonRepository>();

var app = builder.Build();

app.MapGrpcService<PersonService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");


if (app.Environment.IsDevelopment())
{
    app.MapGrpcReflectionService();
}

app.Run();